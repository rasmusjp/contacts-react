import * as React from 'react';
import * as ReactDOM from 'react-dom';
import {useStrict} from 'mobx';
import {Provider} from 'mobx-react';
import {contacts} from './stores';
import App from './App';
import './index.css';

useStrict(true);

ReactDOM.render(
  <Provider contacts={contacts}>
    <App />
  </Provider>,
  document.getElementById('root') as HTMLElement
);
