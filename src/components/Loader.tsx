import * as React from 'react';

export interface LoaderProps {
  show: boolean;
  className?: string;
}

export class Loader extends React.Component<LoaderProps, null> {
  render() {
    const {className, show, children} = this.props;
    return (
      <div className={className}>
        {show &&
          <div className="Loader">Loading...</div>}
  
        {!show && children}
      </div>
    );
  }
}