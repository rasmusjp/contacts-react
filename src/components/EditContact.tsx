import * as React from 'react';
import {Contact} from '../models/contact';
import * as styles from './EditContact.css';

export interface EditContactProps {
  title: string;
  contact?: Contact;
  onSubmit: (contact: Contact) => void;
  onCancel: () => void;
}

interface EditContactState extends Contact {
}

export class EditContact extends React.Component<EditContactProps, EditContactState> {
  constructor(props: EditContactProps) {
    super(props);

    this.state = this.props.contact || {
      name: '',
      phone: '',
      age: 0,
    } as Contact;
  }

  handleSubmit(e: React.FormEvent<HTMLFormElement>) {
    e.preventDefault();
    if (e.currentTarget.checkValidity()) {
      this.props.onSubmit({
        id: this.state.id,
        name: this.state.name,
        age: parseInt(this.state.age.toLocaleString(), 10),
        humor: this.state.humor,
        iq: this.state.iq ? parseInt(this.state.iq.toLocaleString(), 10) : undefined,
        phone: this.state.phone,
        speed: this.state.speed,
        strength: this.state.strength
      });
    }
  }

  handleCancel(e: React.FormEvent<HTMLButtonElement>) {
    e.preventDefault();
    this.props.onCancel();
  }

  handleChange(e: React.FormEvent<HTMLInputElement>) {
    let change = {};
    change[e.currentTarget.name] = e.currentTarget.value;
    this.setState(change);
  }

  render() {
    return (
      <form className={styles.editContact} onSubmit={this.handleSubmit.bind(this)}>
        <h1>{this.props.title}</h1>
        <label>
          Name:
          <input
            type="text"
            name="name"
            required
            value={this.state.name}
            onChange={this.handleChange.bind(this)}
          />
        </label>
        <label>
          Phone:
          <input
            type="tel"
            name="phone"
            required
            value={this.state.phone}
            onChange={this.handleChange.bind(this)}
          />
        </label>
        <label>
          Age:
          <input
            type="number"
            name="age"
            required
            value={this.state.age}
            onChange={this.handleChange.bind(this)}
          />
        </label>
        <button type="button" onClick={this.handleCancel.bind(this)}>Cancel</button>
        <button>Submit</button>
      </form>
    );
  }
}