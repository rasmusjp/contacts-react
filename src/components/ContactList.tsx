import * as React from 'react';
import {observer} from 'mobx-react';
import {Contact} from '../models/contact';
import * as styles from './ContactList.css';

export interface ContactListProps {
  contacts: Contact[];
  onEdit: (contact: Contact) => void;
  onDelete: (contact: Contact) => void;
}

@observer
export class ContactList extends React.Component<ContactListProps, {}> {
  edit(contact: Contact, e: Event) {
    e.preventDefault();
    this.props.onEdit(contact);
  }

  delete(contact: Contact, e: Event) {
    e.preventDefault();

    if (confirm(`Are you sure you want to delete ${contact.name}`)) {
      this.props.onDelete(contact);
    }
  }

  render() {
    const createContactRow = (contact: Contact) =>
      <tr key={contact.id}>
        <td>{contact.name}</td>
        <td>{contact.age}</td>
        <td>{contact.phone}</td>
        <td>
          <a href="#" onClick={this.edit.bind(this, contact)}>Edit</a>
          &nbsp;
          <a href="#" onClick={this.delete.bind(this, contact)}>Delete</a>
        </td>
      </tr>;

    return (
      <table className={styles.contactList}>
        <thead>
          <tr>
            <th>Name</th>
            <th>Age</th>
            <th>Phone</th>
            <th />
          </tr>
        </thead>
        <tbody>
          {this.props.contacts.map(createContactRow)}
        </tbody>
      </table>
    );
  }
}