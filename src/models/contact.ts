export interface Contact {
  id?: string;
  name: string;
  phone: string;
  age: number;
  strength: number;
  iq?: number;
  speed: number;
  humor: boolean;
}