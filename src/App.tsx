import * as React from 'react';
import {observer, inject} from 'mobx-react';
import {Loader} from './components/Loader';
import {EditContact} from './components/EditContact';
import {ContactList} from './components/ContactList';
import {Contact} from './models/contact';
import {Contacts} from './stores/Contacts';

import * as styles from './App.css';

const logo = require('./logo.svg');

interface AppProps {
  contacts?: Contacts;
}

enum Mode {
  list,
  show,
  create,
  edit
}

interface AppState {
  mode: Mode;
  currentContact?: Contact;
}

@inject('contacts')
@observer
class App extends React.Component<AppProps, AppState> {
  constructor(props: AppProps) {
    super(props);
    props.contacts!.loadContacts();

    this.state = { mode: Mode.list };
  }

  handleSave(contact: Contact) {
    this.props.contacts!.save(contact);
    this.setState({ currentContact: undefined });
    this.changeMode(Mode.list);
  }

  handleCancel() {
    this.setState({ currentContact: undefined });
    this.changeMode(Mode.list);
  }

  handleEdit(contact: Contact) {
    this.setState({ currentContact: contact });
    this.changeMode(Mode.edit);
  }

  handleDelete(contact: Contact) {
    this.setState({ currentContact: undefined });
    this.props.contacts!.delete(contact);
  }

  changeMode(mode: Mode) {
    this.setState({ mode: mode });
  }

  render() {
    const {contacts} = this.props;
    const {mode} = this.state;

    return (
      <div className={styles.app}>
        <div className={styles.appHeader}>
          <img src={logo} className={styles.appLogo} alt="logo" />
          <h2>Welcome to React</h2>
        </div>
        <p className={styles.appIntro}>
          To get started, edit <code>src/App.tsx</code> and save to reload.
        </p>
        <p>
          { mode === Mode.list &&
            <button
              type="button"
              onClick={this.changeMode.bind(this, Mode.create)}
            >
              Add contact
            </button>
          }
        </p>
        {mode === Mode.list &&
          <Loader show={contacts!.isLoading}>
            <ContactList
              contacts={Array.from(contacts!.all)}
              onEdit={this.handleEdit.bind(this)}
              onDelete={this.handleDelete.bind(this)}
            />
          </Loader>
        }
        {(mode === Mode.create || mode === Mode.edit) &&
          <EditContact
            title={this.state.currentContact ? 'Edit contact' : 'Create contact'}
            contact={this.state.currentContact}
            onSubmit={this.handleSave.bind(this)}
            onCancel={this.handleCancel.bind(this)}
          />
        }
      </div>
    );
  }
}

export default App;
