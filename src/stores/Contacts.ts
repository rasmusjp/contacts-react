import {action, computed, observable, runInAction} from 'mobx';
import {Contact} from '../models/contact';
import {Fb} from './Firebase';

export class Contacts {
  @observable isLoading = false;
  @observable private contacts = observable.map<Contact>({});

  @computed get all(): Contact[] {
    return this.contacts.values();
  }

  @action loadContacts() {
    this.isLoading = true;

    Fb.contacts.on('value', (snapshot) => {
      runInAction(() => {
        this.contacts.replace(snapshot!.val());
        this.isLoading = false;
      });
    });
  }

  @action save(contact: Contact) {
    if (!contact.id) {
      contact.id = Fb.contacts.push().key!;
    }

    // Firebase does'nt like undefined values
    // so we call JSON.Parse(JSON.stringify()) which removes them
    Fb.contacts.update({[contact.id]: JSON.parse(JSON.stringify(contact))});
  }

  @action delete(contact: Contact) {
    Fb.contacts.child(contact.id!).remove();
  }
}