import * as firebase from 'firebase/app';
import 'firebase/database';

// Initialize Firebase
var config = {
  apiKey: 'AIzaSyB8fNfZXlhI5wCsPLPYdaq5__bqJtmImt0',
  authDomain: 'contacts-ac433.firebaseapp.com',
  databaseURL: 'https://contacts-ac433.firebaseio.com',
  storageBucket: 'contacts-ac433.appspot.com',
  messagingSenderId: '716136867391'
};
firebase.initializeApp(config);

const root = firebase.database().ref();
const contacts = firebase.database().ref('contacts');

export const Fb = {
  root,
  contacts
};